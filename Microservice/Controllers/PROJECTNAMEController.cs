using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using PIT.Labs.ApiResponseLib;

// this is just an example controller file
namespace PROJECTNAME.Controllers
{
    [ApiController]
    [Route("eLearning/PROJECTNAME/GetExample")]
    public class GetExample : ControllerBase
    {
        private IExampleApi exampleApi { get; set; }

        public GetExample(IExampleApi exampleApi)
        {
            this.exampleApi = exampleApi;
        }

        [HttpGet]
        public ActionResult<ApiResponse<string>> Get([FromQuery] string some_val)
        {
            try
            {
                var output = exampleApi.GetExample(some_val)
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<string>(e);
            }
        }
    }

    [ApiController]
    [Route("eLearning/PROJECTNAME/GetExampleList")]
    public class GetExampleList : ControllerBase
    {
        private ExampleFactory exampleFactory { get; set; }

        public GetExampleList(ExampleFactory exampleFactory)
        {
            this.exampleFactory = exampleFactory;
        }

        [HttpGet]
        public ActionResult<ApiResponse<List<ExampleItem>>> Get()
        {
            try
            {
                var output = moodleFactory.GetExampleList();
                return ApiResponseFactory.Create(output);
            }
            catch (Exception e)
            {
                return ApiResponseFactory.CreateErrorFromException<List<ExampleItem>>(e);
            }
        }
    }

}
