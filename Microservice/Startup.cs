using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Targets;
using NLog.Config;
using NLog.Targets.GraylogHttp;
using System.Reflection;


// TODO: replace with project name
namespace PROJECTNAME
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO: inject your interfaces and classes
            services.AddSingleton<IExample, Example>();
            services.AddSingleton<ExampleFactory, ExampleFactory>();
            services.AddControllers();
            // configure logging
            var config = new LoggingConfiguration();
            // add logging to console
            var consoleTarget = new ConsoleTarget("console");
            config.AddTarget(consoleTarget);
            config.AddRuleForAllLevels(consoleTarget); // all to console
            // add logging to graylog
            bool graylog_enabled = "true".Equals(
                    Environment.GetEnvironmentVariable("GRAYLOG_ENABLED"),
                    StringComparison.OrdinalIgnoreCase);
            if (graylog_enabled)
            {
                Console.WriteLine("Graylog enabled");
                var graylogTarget = new GraylogHttpTarget()
                {
                    GraylogServer = Environment.GetEnvironmentVariable("GRAYLOG_SERVER"),
                                  GraylogPort = Environment.GetEnvironmentVariable("GRAYLOG_PORT"),
                                  Facility = Environment.GetEnvironmentVariable("GRAYLOG_FACILITY"),
                };
                // https://github.com/dustinchilson/NLog.Targets.GraylogHttp/issues/98
                graylogTarget.Name = "graylog";
                config.AddTarget(graylogTarget);
                config.AddRuleForAllLevels(graylogTarget); // all to console
            } else {
                Console.WriteLine("Graylog disabled");
            }
            // save new config
            LogManager.Configuration = config;
            var version = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            Console.WriteLine($"Running Version: {version}");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
