# MicroserviceTemplate

A template for new microservices.

## Setup
1. Make todos in these files:
    - IntegrationTest/dockerfile
    - IntegrationTest/PROJECTNAMEIntegrationTest.csproj
    - UnitTest/dockerfile
    - UnitTest/PROJECTNAMEUnitTest.csproj
    - Microservice/dockerfile
    - Microservice/PROJECTNAME.csproj
2. Add projectname and make todos in these files:
    - Microservice/Controllers/PROJECTNAMEController.cs
    - Microservice/Program.cs
    - Microservice/Startup.cs
3. Rename files that contains the projectname:
    - Microservice/PROJECTNAME.csproj
    - Microservice/Controllers/PROJECTNAMEController.cs
    - IntegrationTest/PROJECTNAMEIntegrationTest.csproj
    - UnitTest/PROJECTNAMEUnitTest.csproj
4. Create solution: `dotnet new sln`
5. Add projects to solution: `dotnet sln add **/*.csproj`
6. Change the name of the variable `PROJECTNAME_TAG` in [.gitlab_ci.yml](.gitlab_ci.yml)

## Add Microservice to Compose Project
1. Checkout [the Compose-Project](https://git.rwth-aachen.de/rwthappapimicroservices/Compose/)
2. Add your microservice in the following files:
    - docker-compose.yml
    - docker-compose-testing.yml
    - docker-compose-inttest.yml
3. Add the new variable that you created in step 6 in [Setup](#setup) to the method `print_tags` in [Compose/inttest.sh](https://git.rwth-aachen.de/rwthappapimicroservices/Compose/-/blob/master/inttest.sh)
4. Now pushing the changes starts a pipeline that runs all integration tests including the ones from the new project


## Last steps
1. Go to Settings -> CI/CD -> "Container Registry tag expiration policy" and enable it for the Regex "xtmp-.*"
